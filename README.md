Summary
=======

This is the README for the DuMuX code as used for Elena Shigorina's MSc Thesis.

In the result folder are the simulation-end-time vtu files used to generate the 
plots in the thesis.

The Injection data file for the "Real" scenario is not included. 

Hints for simulating:
If the simulation breaks, try a restart with a different Newton tolerance or
even better with different (smaller) maximum timestep sizes, 
especially during Ca-rich or inoculation injections.


Installation
============


The easiest way to install this module is to create a new folder and to execute the file 
[installShigorina2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Shigorina2014a/raw/master/installShigorina2014a.sh) 
in this folder.

```bash
mkdir -p Shigorina2014a && cd Shigorina2014a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Shigorina2014a/raw/master/installShigorina2014a.sh
sh ./installShigorina2014a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/co2/biomin_thesis/,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/co2/biomin_thesis/biomin.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.


Used Versions and Software
==========================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

dumux...................: version 2.5-svn (/temp/elenas/DUMUX/dumux)
dune-common.............: version 2.2.1 (/temp/elenas/DUMUX/dune-common)
dune-geometry...........: version 2.2.1 (/temp/elenas/DUMUX/dune-geometry)
dune-grid...............: version 2.2.1 (/temp/elenas/DUMUX/dune-grid)
dune-istl...............: version 2.2.1 (/temp/elenas/DUMUX/dune-istl)
dune-localfunctions.....: version 2.2.1 (/temp/elenas/DUMUX/dune-localfunctions)
ALBERTA.................: no
ALUGrid.................: no
AlgLib for DUNE.........: no
AmiraMesh...............: no
BLAS....................: yes
GMP.....................: yes
Grape...................: no
METIS...................: no
MPI.....................: yes (OpenMPI)
OpenGL..................: yes (add GL_LIBS to LDADD manually, etc.)
ParMETIS................: no
SuperLU-DIST............: no
SuperLU.................: no
UG......................: yes (sequential)
psurface................: no
