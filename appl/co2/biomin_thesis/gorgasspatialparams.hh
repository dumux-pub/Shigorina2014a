  // $Id$
/*****************************************************************************
 *   Copyright (C) 2008-2010 by Andreas Lauser                               *
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_BIOMIN_SPATIAL_PARAMS_HH
#define DUMUX_BIOMIN_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>


namespace Dumux
{
//forward declaration
template<class TypeTag>
class GorgasSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(GorgasSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(GorgasSpatialParams, SpatialParams, Dumux::GorgasSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(GorgasSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};
}

/**
 * \brief Definition of the spatial parameters for the brine-co2 problem
 *
 */
template<class TypeTag>
class GorgasSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename Grid::ctype CoordScalar;
    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
    };

//    typedef typename GET_PROP_TYPE(TypeTag, TwoPNCMinIndices)) Indices;
    enum {
        lPhaseIdx = FluidSystem::lPhaseIdx,
        gPhaseIdx = FluidSystem::gPhaseIdx,
    };

    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> Vector;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GridView::template Codim<0>::Entity Element;

public:
    GorgasSpatialParams(const GridView &gv)
        : ParentType(gv)
    {

    	try
    	{
    	porosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);
    	critPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, CritPorosity);
    	fracPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, FracPorosity);
    	ScalarRockK_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, RockPermeability);
    	ScalarFracK_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, FracPermeability);
    	fracWidth_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, FracWidth);
    	rFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, FracRadius);
    	zmax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, TotalHeight);

    	}
        catch (Dumux::ParameterException &e)
        {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }

        // intrinsic permeabilities

                rockK_ = Scalar(0.0);
		fracK_ = Scalar(0.0);
		//ScalarRockK_ = 1.0856e-14;			//Al: permeability 11mD
		//ScalarFracK_ = 1.e-11;			//Al: 10000 mD
			  for (int i = 0; i < dim; i++)
			  {
				  rockK_[i][i] = ScalarRockK_;
				  fracK_[i][i] = ScalarFracK_;
			  }

        // porosity:
				  //porosity_ = 0.15;		//Al: porosity 15%
				  //fracPorosity_ = 0.88;
				  //critPorosity_ = 0.0;

        // residual saturations
				rockMaterialParams_.setSwr(0.2);
				rockMaterialParams_.setSnr(0.05);

        // parameters for the Brooks-Corey law
				rockMaterialParams_.setPe(1e4);
				rockMaterialParams_.setLambda(2.0);
    }

    ~GorgasSpatialParams()
    {}


    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution The global solution vector
     */
    void update(const SolutionVector &globalSolution)
    {
    };

    /*!
     * \brief Apply the intrinsic permeability tensor to a pressure
     *        potential gradient.
     *
     * \param element The current finite element
     * \param fvElemGeom The current finite volume geometry of the element
     * \param scvfIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    const Tensor &intrinsicPermeability(const Element &element,
                                       const FVElementGeometry &fvGeometry,
                                       int scvIdx) const
    {
    	const GlobalPosition &pos = fvGeometry.subContVol[scvIdx].global;
        if(isFrac_(pos)==true)
        	return fracK_;

        else
        	return rockK_;

    }
    const Scalar &intrinsicPermeabilityScalar(const Element &element,
                                       const FVElementGeometry &fvGeometry,
                                       int scvIdx) const
    {
    	const GlobalPosition &pos = fvGeometry.subContVol[scvIdx].global;
        if(isFrac_(pos)==true)
        	return ScalarFracK_;

        else
        	return ScalarRockK_;

    }

    /*!
     * \brief Define the porosity \f$[-]\f$ of the spatial parameters
     *
     * \param vDat The data defined on the sub-control volume
     * \param element The finite element
     * \param fvElemGeom The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the porosity needs to be defined
     */
    double solidity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    int scvIdx) const
    {
    	const GlobalPosition &pos = fvGeometry.subContVol[scvIdx].global;
        if(isFrac_(pos)==true)
        	return 1 - fracPorosity_;

        else
        	return 1 - porosity_;

    }
    const Scalar porosity(const Element &element,				//TODO sonst gibt es einen Fehler: Dune reported error: Dune::InvalidStateException [porosityAtPos:/temp/hommel/DUMUX/dumux/dumux/material/spatialparams/boxspatialparams1p.hh:167]: The spatial parameters do not provide a porosityAtPos() method.
            const FVElementGeometry &fvGeometry,
            int scvIdx) const
    {
    	const GlobalPosition &pos = fvGeometry.subContVol[scvIdx].global;
        if(isFrac_(pos)==true)
    		return fracPorosity_;

    	else
    		return porosity_;
    }
    const Scalar critPorosity(const Element &element,				
            const FVElementGeometry &fvGeometry,
            int scvIdx) const
    {
        return critPorosity_;
    }
//    Scalar porosityAtPos(const GlobalPosition& globalPos) const
//    {
//        DUNE_THROW(Dune::InvalidStateException,
//                   "The spatial parameters do not provide "
//                   "a porosityAtPos() method.");
//    }
    // return the brooks-corey context depending on the position
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
    {
            return rockMaterialParams_;
    }

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvElemGeom The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    double heatCapacity(const Element &element,
//                        const FVElementGeometry &fvElemGeom,
//                        int scvIdx) const
//    {
//        return
//            790 // specific heat capacity of granite [J / (kg K)]
//            * 2700 // density of granite [kg/m^3]
//            * (1 - porosity(element, fvElemGeom, scvIdx));
//    }


   /* double heatCapacity(const Element &element,
                            const FVElementGeometry &fvElemGeom, int scvIdx, Scalar phasePressure) const
        {
            return 0;
        }*/

    /*!
     * \brief Calculate the heat flux \f$[W/m^2]\f$ through the
     *        rock matrix based on the temperature gradient \f$[K / m]\f$
     *
     * This is only required for non-isothermal models.
     *
     * \param heatFlux The result vector
     * \param tempGrad The temperature gradient
     * \param element The current finite element
     * \param fvElemGeom The finite volume geometry of the current element
     * \param scvfIdx The local index of the sub-control volume face where
     *                    the matrix heat flux should be calculated
     */
//    void matrixHeatFlux(Vector &heatFlux,
//                        const FluxVariables &fluxDat,
//                        const ElementVolumeVariables &vDat,
//                        const Vector &tempGrad,
//                        const Element &element,
//                        const FVElementGeometry &fvElemGeom,
//                        int scvfIdx) const
//    {
//        static const Scalar lWater = 0.6;
//        static const Scalar lGranite = 2.8;
//
//        // arithmetic mean of the liquid saturation and the porosity
//        const int i = fvElemGeom.subContVolFace[scvfIdx].i;
//        const int j = fvElemGeom.subContVolFace[scvfIdx].j;
//        Scalar Sl = std::max(0.0, (vDat[i].saturation(lPhaseIdx) +
//                                     vDat[j].saturation(lPhaseIdx)) / 2);
//        Scalar poro = (porosity(element, fvElemGeom, i) +
//                       porosity(element, fvElemGeom, j)) / 2;
//
//        Scalar lsat = pow(lGranite, (1-poro)) * pow(lWater, poro);
//        Scalar ldry = pow(lGranite, (1-poro));
//
//        // the heat conductivity of the matrix. in general this is a
//        // tensorial value, but we assume isotropic heat conductivity.
//        Scalar heatCond = ldry + sqrt(Sl) * (ldry - lsat);
//
//        // the matrix heat flux is the negative temperature gradient
//        // times the heat conductivity.
//        heatFlux = tempGrad;
//        heatFlux *= -heatCond;
//    }

private:
    bool isFrac_(const GlobalPosition &pos) const
       {
    	return (pos[dim-1] >= (zmax_/2 - fracWidth_/2) && pos[dim-1] && pos[dim-1]<= (zmax_/2 + fracWidth_/2) && pos[1]*pos[1]+pos[0]*pos[0] <= (rFrac_)*(rFrac_));
//    	return (pos[dim-1] >= (zmax_ - fracWidth_)/2  && pos[dim-1] <= (zmax_ + fracWidth_)/2);
       };

    Dune::FieldMatrix<Scalar,dim,dim> rockK_;
    Dune::FieldMatrix<Scalar,dim,dim> fracK_;
//    static constexpr Scalar fracPermFactor_ = 1e3;
//    static constexpr Scalar zmax_ = 0.1905;	//0.381; //[m]
//    static constexpr Scalar fracWidth_ = 0.1; //[m] 0.00635;	//0.0127; //[m]
//    static constexpr Scalar tan_frac_angle_ = 0.1763; //[] = tan(10°) half of the fracture (22° ~~ 20°) is in the domain
//    static constexpr Scalar r_frac = 7.5; // [m] [] = tan(20°) half of the fracture (41° ~~ 40°) is in the domain
    Scalar porosity_;
    Scalar fracPorosity_;
    Scalar critPorosity_;
    Scalar ScalarRockK_;
    Scalar ScalarFracK_;
    Scalar fracWidth_ ;
    Scalar rFrac_;
    Scalar zmax_;

    MaterialLawParams rockMaterialParams_;
};

}

#endif  
