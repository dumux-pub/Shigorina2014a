// $Id: 2p-nc-propertydefaults.hh 3784 2010-06-24 13:43:57Z bernd $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPNCModel
 */
/*!
 * \file
 *
 * \brief Defines default values for most properties required by the
 *        2pnc box model.
 */
#ifndef DUMUX_2PNC_PROPERTY_DEFAULTS_HH
#define DUMUX_2PNC_PROPERTY_DEFAULTS_HH

#include "2pncindices.hh"
#include "2pncmodel.hh"
#include "2pncindices.hh"
#include "2pncfluxvariables.hh"
#include "2pncvolumevariables.hh"
#include "2pncproperties.hh"
#include "2pncnewtoncontroller.hh"


#include <dumux/implicit/common/implicitdarcyfluxvariables.hh>
#include <dumux/material/spatialparams/implicitspatialparams.hh>

namespace Dumux
{

namespace Properties {
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

/*!
 * \brief Set the property for the number of components.
 *
 * We just forward the number from the fluid system
 *
 */



SET_PROP(BoxTwoPNC, NumComponents)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents;

};

SET_PROP(BoxTwoPNC, ReplaceCompEqIdx)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents;
};

SET_PROP(BoxTwoPNC, NumMajorComponents)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numPhases;
    static_assert(value == 2,
                  "Only fluid systems with 2 fluid phases consisting of 2 different major components are supported by the 2p-nc-Min model!");
};

/*!
 * \brief Set the property for the number of fluid phases.
 *
 * We just forward the number from the fluid system and use an static
 * assert to make sure it is 2.
 */
SET_PROP(BoxTwoPNC, NumPhases)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numPhases;
    static_assert(value == 2,
                  "Only fluid systems with 2 fluid phases are supported by the 2p-nc model!");
};


/*!
 * \brief Set the property for the number of equations. For each component and each mineral phase one equation has to
 * be solved.
 */
SET_PROP(BoxTwoPNC, NumEq)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents;
};

//! Set the default formulation to pl-Sg
SET_INT_PROP(BoxTwoPNC, Formulation, TwoPNCFormulation::plSg);

/*!
 * \brief Set the property for the material parameters by extracting
 *        it from the material law.
 */
SET_PROP(BoxTwoPNC, MaterialLawParams)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(MaterialLaw)) MaterialLaw;

public:
    typedef typename MaterialLaw::Params type;
};

//! Use the 2p-3c-Min local jacobian operator for the 2p-3c-Min model
SET_TYPE_PROP(BoxTwoPNC,
              LocalResidual,
              TwoPNCLocalResidual<TypeTag>);

//! Use the 2p-3c-Min specific newton controller for the 2p-nc model
SET_TYPE_PROP(BoxTwoPNC, NewtonController, TwoPNCNewtonController<TypeTag>);

//! the Model property
SET_TYPE_PROP(BoxTwoPNC, Model, TwoPNCModel<TypeTag>);

//! the VolumeVariables property
SET_TYPE_PROP(BoxTwoPNC, VolumeVariables, TwoPNCVolumeVariables<TypeTag>);

//! the FluxVariables property
SET_TYPE_PROP(BoxTwoPNC, FluxVariables, TwoPNCFluxVariables<TypeTag>);

//! define the base flux variables to realize Darcy flow
SET_TYPE_PROP(BoxTwoPNC, BaseFluxVariables, ImplicitDarcyFluxVariables<TypeTag>);

//! the upwind weight for the mass conservation equations.
SET_SCALAR_PROP(BoxTwoPNC, ImplicitMassUpwindWeight, 1.0);

//! the upwind factor for the mobility.
SET_SCALAR_PROP(BoxTwoPNC, ImplicitMobilityUpwindWeight, 1.0);

//! The indices required by the isothermal 2p-3c-Min model
SET_TYPE_PROP(BoxTwoPNC, Indices, TwoPNCIndices <TypeTag, /*PVOffset=*/0>);

//SpatialParameters property
SET_TYPE_PROP(BoxTwoPNC, SpatialParams, ImplicitSpatialParams<TypeTag>);

// enable gravity by default
SET_BOOL_PROP(BoxTwoPNC, ProblemEnableGravity, true);

// disable velocity output by default
SET_BOOL_PROP(BoxTwoPNC, VtkAddVelocity, false);

// disable electro chemistry vtk output by default
SET_BOOL_PROP(BoxTwoPNC, useElectrochem, false);

//
}

}

#endif
