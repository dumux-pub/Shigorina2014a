
install(FILES
        2pbiominfluxvariables.hh
        2pbiominlocalresidual.hh
        2pbiominmodel.hh
        2pbiominvolumevariables.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/implicit/2pbiomin)
